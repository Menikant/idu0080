namespace Model
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class EntityModel : DbContext
    {
        public EntityModel()
            : base("name=AppDBConnectionString") {
        }

        public EntityModel(string pConnectionString):
            base (pConnectionString)
        {
            Configuration.ProxyCreationEnabled = false;
        }

        public virtual DbSet<Course> Course { get; set; }
        public virtual DbSet<CourseStudents> CourseStudents { get; set; }
        public virtual DbSet<Student> Student { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
        }
    }
}
