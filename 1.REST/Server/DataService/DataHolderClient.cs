﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Web;
using Model;
using Server.Properties;
using Service;

namespace Server.DataService
{
    public class DataHolderClient
    {
        private IDataHolder channelDataHolder;
        public DataHolderClient() {
            var webBinding = new WebHttpBinding();
            EndpointAddress myEndpoint = new EndpointAddress(Settings.Default.DataServiceEndpoint);
            var channel = new ChannelFactory<IDataHolder>(webBinding, myEndpoint);
            channel.Endpoint.Behaviors.Add(new WebHttpBehavior());
            this.channelDataHolder = channel.CreateChannel();
        }


        public List<Course> GetCourses() {
            using (new OperationContextScope((IContextChannel)channelDataHolder))
                return channelDataHolder.GetCourses();
        }

        public List<Student> GetStudents(string pFilter) {
            using (new OperationContextScope((IContextChannel)channelDataHolder))
                return channelDataHolder.GetStudents(pFilter);
        }
    }
}