﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace Server.Helpers
{
    public static class ConverterUtil
    {
        public static int GetNumValure(this string stringNum){
            var errorMessage = "Invalid parameter format";
            if (String.IsNullOrEmpty(stringNum))
                throw new FaultException(errorMessage);

            try{
                return Int32.Parse(stringNum);
            }
            catch (Exception){
                throw new FaultException(errorMessage);
            }
        }
    }
}