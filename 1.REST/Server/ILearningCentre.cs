﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Model;

namespace Server
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILearningCentre" in both code and config file together.
    [ServiceContract]
    public interface ILearningCentre
    {
        [OperationContract]
        [WebGet(ResponseFormat =WebMessageFormat.Json ,RequestFormat = WebMessageFormat.Json)]
        List<Course> GetCourses();

        [OperationContract]
        [WebGet(UriTemplate = "GetStudents/{pFilter}", ResponseFormat = WebMessageFormat.Json, RequestFormat = WebMessageFormat.Json)]
        List<Student> GetStudents(string pFilter);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Students/{pStudentId}", ResponseFormat = WebMessageFormat.Json)]
        Student GetStudent(string pStudentId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Courses/{pCourseId}", ResponseFormat = WebMessageFormat.Json)]
        Course GetCourse(string pCourseId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "CourseParticipants/{pCourseId}", ResponseFormat = WebMessageFormat.Json)]
        List<Student> GetCourseStudents(string pCourseId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Register/{pCourseId}/{pStudentId}")]
        void RegisterStudend(string pCourseId, string pStudentId);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Remove/{pCourseId}/{pStudentId}")]
        void RemoveFromCourse(string pCourseId, string pStudentId);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "AddStudent/", RequestFormat = WebMessageFormat.Json)]
        void AddUpdateStudent(Student pStudent);

        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "AddCourse/", RequestFormat = WebMessageFormat.Json)]
        void AddUpdateCourse(Course pCourse);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "DeleteStudent/{pStudentId}")]
        void DeleteStudent(string pStudentId);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "DeleteCourse/{pCourseId}")]
        void DeleteCourse(string pCourseId);

    }
}
