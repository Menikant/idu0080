﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Model;
using Server.DataService;
using Server.Helpers;
using Server.Properties;

namespace Server
{
    public class LearningCentre : ILearningCentre
    {
        DataHolderClient client = new DataHolderClient();

        public List<Course> GetCourses()
        {
            return client.GetCourses();
        }

        public List<Student> GetStudents(string pFilter)
        {
            return client.GetStudents(pFilter);
        }

        public Course GetCourse(string pCourseId)
        {
            var courseId = pCourseId.GetNumValure();
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                return entities.Course.Find(courseId);
            }
        }

        public Student GetStudent(string pStudentId)
        {
            var studentId = pStudentId.GetNumValure();
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                return entities.Student.Find(studentId);
            }
        }

        public List<Student> GetCourseStudents(string pCourseId)
        {
            var courseId = pCourseId.GetNumValure();
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                if (entities.CourseStudents.Any(c => c.CourseId == courseId))
                    return entities.CourseStudents
                        .Where(c => c.CourseId == courseId)
                        .Select(c => c.Student).ToList();
                else
                    return new List<Student>();
            }
        }

        public void RegisterStudend(string pCourseId, string pStudentId)
        {
            var courseId = pCourseId.GetNumValure();
            var studentId = pStudentId.GetNumValure();
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                if (entities.CourseStudents.Any(c =>
                        c.CourseId == courseId &&
                        c.StudentId == studentId))
                    throw new FaultException("A student is already registered to this course.");

                entities.CourseStudents.Add(new CourseStudents
                {
                    CourseId = courseId,
                    StudentId = studentId
                });

                entities.SaveChanges();
            }
        }

        public void RemoveFromCourse(string pCourseId, string pStudentId)
        {
            var courseId = pCourseId.GetNumValure();
            var studentId = pStudentId.GetNumValure();
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                var courseRefistration = entities.CourseStudents
                    .FirstOrDefault(c =>
                        c.CourseId == courseId &&
                        c.StudentId == studentId);
                if (courseRefistration != null)
                {
                    entities.CourseStudents.Remove(courseRefistration);
                    entities.SaveChanges();
                }
                else
                {
                    throw new FaultException("A student is not registered to this course.");
                }
            }
        }

        public void AddUpdateStudent(Student pStudent)
        {
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                if (pStudent.Id != 0)
                {
                    Student entity = entities.Student.FirstOrDefault(s => s.Id == pStudent.Id);
                    if (entity != null)
                    {
                        entities.Entry(entity).CurrentValues.SetValues(pStudent);
                        entities.SaveChanges();
                    }
                }
                else
                {
                    entities.Student.Add(pStudent);
                    entities.SaveChanges();
                }
            }
        }

        public void AddUpdateCourse(Course pCourse)
        {
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                if (pCourse.Id != 0)
                {
                    Course entity = entities.Course.FirstOrDefault(s => s.Id == pCourse.Id);
                    if (entity != null)
                    {
                        entities.Entry(entity).CurrentValues.SetValues(pCourse);
                        entities.SaveChanges();
                    }
                }
                else
                {
                    entities.Course.Add(pCourse);
                    entities.SaveChanges();
                }
            }
        }

        public void DeleteStudent(string pStudentId)
        {
            var studentId = pStudentId.GetNumValure();
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                var student = entities.Student
                    .FirstOrDefault(s => s.Id == studentId);
                if (student != null)
                {
                    entities.Student.Remove(student);
                    entities.SaveChanges();
                }
                else
                {
                    throw new FaultException("A student is not found.");
                }
            }
        }

        public void DeleteCourse(string pCourseId)
        {
            var courseId = pCourseId.GetNumValure();
            using (var entities = new EntityModel(Settings.Default.DatabaseConnectionString))
            {
                var course = entities.Course
                    .FirstOrDefault(c => c.Id == courseId);
                if (course != null)
                {
                    entities.Course.Remove(course);
                    entities.SaveChanges();
                }
                else
                {
                    throw new FaultException("A student is not found.");
                }
            }
        }
    }
}
