﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Model;
using Service.Properties;

namespace Service{
    public class DataHolder : IDataHolder
    {
        public List<Course> GetCourses(){
            try {
                using (var entities = new EntityModel(Settings.Default.StorageConnactionString)) {
                    return entities.Course.ToList();
                }
            }
            catch (Exception e) {
                throw new FaultException(e.Message);
            }
        }

        public List<Student> GetStudents(string pFilter) {
            try {
                using (var entities = new EntityModel(Settings.Default.StorageConnactionString)) {
                    if (string.IsNullOrEmpty(pFilter))
                        return entities.Student.ToList();

                    return entities.Student.Where(c =>
                        c.FirstName.Contains(pFilter) ||
                        c.LastName.Contains(pFilter))
                    .Select(c => c).ToList();

                }
            }
            catch (Exception e) {
                throw new FaultException(e.Message);
            }
        }
    }
}
