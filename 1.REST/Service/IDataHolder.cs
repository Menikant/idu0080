﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using Model;

namespace Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDataHolder" in both code and config file together.
    [ServiceContract]
    public interface IDataHolder{       
        [OperationContract,WebGet(ResponseFormat = WebMessageFormat.Json)]
        List<Course> GetCourses();

        [OperationContract, WebGet(UriTemplate = "GetStudents/{pFilter}", ResponseFormat = WebMessageFormat.Json)]
        List<Student> GetStudents(string pFilter);
    }
}

